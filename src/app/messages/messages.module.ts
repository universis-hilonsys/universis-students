import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagesListComponent } from './components/messages-list/messages-list.component';
import { MessagesHomeComponent } from './components/messages-home/messages-home.component';
import {MessagesRouting} from './messages.routing';
import {environment} from '../../environments/environment';
import {TranslateModule , TranslateService} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {RequestsModule} from '../requests/requests.module';

@NgModule({
  imports: [
    CommonModule,
    MessagesRouting,
    TranslateModule,
    FormsModule,
    RequestsModule,
    InfiniteScrollModule
  ],
  providers: [
  ],
  declarations: [MessagesListComponent, MessagesHomeComponent]
})
export class MessagesModule {
  constructor(private _translateService: TranslateService) {
  environment.languages.forEach((culture) => {
    import(`./i18n/messages.${culture}.json`).then((translations) => {
      this._translateService.setTranslation(culture, translations, true);
    });
  });
}
}
