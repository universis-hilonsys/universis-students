import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GradesService} from 'src/app/grades/services/grades.service';
import {GradeScaleService, SharedModule} from '@universis/common';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import { ConfigurationService } from '@universis/common';
import {TranslateModule} from '@ngx-translate/core';

import {StudentRecentGradesComponent} from './student-recent-grades.component';
import {TestingConfigurationService} from '../../../test';

describe('StudentRecentGradesComponent', () => {
    let component: StudentRecentGradesComponent;
    let fixture: ComponentFixture<StudentRecentGradesComponent>;

    const gradeSvc = jasmine.createSpyObj('GradesService', ['getAllGrades', 'getCourseTeachers', 'getGradeInfo',
                                          'getDefaultGradeScale', 'getThesisInfo', 'getLastExamPeriod', 'getRecentGrades']);
    const gradeScaleSvc = jasmine.createSpyObj('GradeScaleService', ['getGradeScales', 'getGradeScale']);

    gradeSvc.getRecentGrades.and.returnValue(Promise.resolve(JSON.parse('{"value":[]}')));

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [StudentRecentGradesComponent],
            imports: [HttpClientTestingModule,
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                }),
              SharedModule
            ],
            providers: [
                {
                    provide: GradesService,
                    useValue: gradeSvc
                },
                {
                    provide: GradeScaleService,
                    useValue: gradeScaleSvc
                },
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                }]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StudentRecentGradesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
